@extends('layouts.app')

@section('script')

<!-- script for time limitation of exam -->
<script type="text/javascript">
var timeoutHandle;
function countdown(minutes) {
    var seconds = 60;
    var mins = minutes
    function tick() {
        var counter = document.getElementById("timer");
        var current_minutes = mins-1
        seconds--;
        counter.innerHTML =
        current_minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds);
        if( seconds > 0 ) {
            timeoutHandle=setTimeout(tick, 1000);
        } else {
            if(mins > 1){
               // countdown(mins-1);   never reach “00″ issue solved:Contributed by Victor Streithorst
               setTimeout(function () { countdown(mins - 1); }, 1000);
            }
        }
    }
    tick();
}
countdown('<?php echo $time; ?>');
</script>

<!-- script for disable url -->
<script type="text/javascript">
    var time= '<?php echo $time; ?>';
    var realtime = time*60000;
    setTimeout(function () {
        alert('Time Out');
        window.location.href= '/';},
   realtime);

</script>

@endsection
@section('content')
    <div class="container">
        @if($topic)
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <h1><b>Time <span id="timer" style="color: red">0.00</span></b></h1><br>
                    <h1>{{$topic->title}}</h1>
                    <form action="{{route('results.store')}}" method="post">
                        @csrf
                        <div>
                            <input type="hidden" name="duration" value="{{$topic->duration}}">

                            <input type="hidden" name="topic_id" value="{{$topic->id}}">
                            @foreach($topic->questions as $question )
                                <div>
                                    {{$question->question_text}}
                                    <input type="hidden" name="question_id[]" value="{{$question->id}}">
                                    @foreach($question->options as $option)
                                        <div>
                                            <input type="checkbox" name="option[{{$question->id}}][{{$option->id}}]"
                                                   value="{{$option->correct}}">
                                            {{$option->option}}
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                        <a href="{{route('results.show', $topic->id)}}"><input type="submit" value="Submit"
                                                                               class="btn btn-success mt-3"></a>
                    </form>
                </div>
            </div>
        @else
            <h1>No Topic</h1>
        @endif
    </div>



@endsection
