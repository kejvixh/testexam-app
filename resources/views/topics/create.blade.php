@extends('layouts.app')

@section('content')
    <div class="d-flex" id="wrapper">
        @include('adminpanel.adminSidebar')
        <div class="container">
            <h2 align="center">Create new Quiz</h2>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group">
                <form action="{{route('topics.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="title" class="form-control" placeholder="title">
                    </div>
                    <div class="form-group">
                        <input type="text" name="question" class="form-control" placeholder="No of questions">
                    </div>
                    <div class="form-group">
                        <input type="text" name="duration" class="form-control" placeholder="duration">
                    </div>
                    <input type="submit" name="submit" id="submit" class="btn btn-info" value="Create Topic"/>
                </form>
            </div>

           
@endsection